# Tools

These were the tools that I used to setup a local kubernetes environment to evaluate
the Elastic Stack.

## Kustomize

This cli takes a group of yaml manifests, adds any templating rules, and produces a 
single combined yaml manifest that defines what state the Kubernetes cluster should
achieve. It makes it much easier to apply manifests to the cluster in one cli command.
This project uses the latest build as the build packaged with `kubectl` is behind on
single cli command features. This is recommended for the use of this project.
See [documentation](https://kustomize.io/) for more info.

## kubectl

This cli tool is required in order to access and control the Kubernetes cluster.
See [documentation](https://kubernetes.io/docs/reference/kubectl/overview/) for more info.

## docker*

The docker runtime engine is optional for running this project. You can apply these
yaml manifests directly to your private or public cloud Kuberentes cluster. The charges
associated will be your responisbility. However, you can install the docker runtime 
locally and then run the [setup scripts](#Setup-script).

## Kubernetes in Docker (kind)*

This is optional for running this project. This cli tool allows you to create a fully 
functional Kubernetes cluster within the docker runtime. It makes it easy to evaluate 
any Kuberentes projects locally. If using this the below is mandatory as well as creating
the cluster with the `kind-config.yaml` file.

## kind Nginx Ingress*

This installs the Nginx ingress controller to the kind cluster. Its needed to allow 
simple forwarding of the `kibana` service to the end users. If not using this optional
controller you can instead port forward the Kubernetes service.

```bash
kubectl port-forward service/kibana 5601:kibana
```

# Setup Script

To setup the local tools effectively and efficiently you can use the following which will
setup all the optional tools and create a kind cluster:

```bash
cd tools
chmod +x ./install.sh
./install.sh
```

**To install tools only**

```bash
cd tools
chmod +x ./install-tools.sh
./install-tools.sh
```

**To create cluster only**

```bash
cd tools
chmod +x ./create-cluster.sh
./create-cluster.sh
```